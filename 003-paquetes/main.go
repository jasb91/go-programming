package main

import (
	"fmt"

	"gitlab.com/jasb91/go-programming/003-paquetes/gato"
)

func main() {
	fmt.Println("Hola desde main()")
	gato.Hola()
	gato.Comen()

}
